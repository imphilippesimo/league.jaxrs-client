package com.philippe.league.jaxrs.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import com.philippe.league.jaxrs.client.entities.League;
import com.philippe.league.jaxrs.client.entities.Player;
import com.philippe.league.jaxrs.client.entities.Team;

/**
 * Hello world!
 *
 */
public class App {

	static Client client = ClientBuilder.newClient();

	public static void main(String[] args) {
		System.out.println("Hello World!");

		League nfl = new League("NAF NAF LEAGUE", "football");
		League league1 = new League("league1", "soccer");
		League nba = new League("nba", "basketball");

		List<League> leagues = new ArrayList<League>(Arrays.asList(nfl, league1, nba));
		List<Team> teams = new ArrayList<Team>();
		List<Player> players = new ArrayList<Player>();

		int nbrEquipes = 10;
		int nbrEquipeParLeague = nbrEquipes / 3;
		int debut = 0;
		int fin = nbrEquipeParLeague;
		Random random = new Random();

		// for (League league : leagues) {
		//
		// for (int i = debut; i < fin; i++) {
		// Team team = new Team("team" + String.valueOf(i), "1ere division", league,
		// "Oakland" + String.valueOf(i));
		//
		// teams.add(team);
		//
		// for (int j = 0; j < 10; j++) {
		// String status = j % 2 == 0 ? "Actif" : "Inactif";
		// Player player = new Player("player" + String.valueOf(j), "Mickael",
		// random.nextInt(), team, status,
		// new Date(random.ints(4).sum()), random.doubles(6).sum());
		// players.add(player);
		// }
		//
		// }
		// Team team1 = new Team("team sans league", "League1", null, "Oakland");
		// teams.add(team1);
		// Player player1 = new Player("player", "Mickael", random.nextInt(), null,
		// "Actif",
		// new Date(random.ints(4).sum()), random.doubles(6).sum());
		// players.add(player1);
		//
		// debut = fin;
		// fin += nbrEquipeParLeague;
		// }

		System.out.println(createPlayer(new Player("lkj", "kljklj", 4, null, "jkhjk", null, 7785D)));
	}

	public List<Player> createPlayers(List<Player> players) {

		List<Player> players2 = new ArrayList<Player>();

		for (Player player : players) {
			player = createPlayer(player);
			players2.add(player);
		}

		return players2;

	}

	public List<Team> createTeams(List<Team> teams) {

		return teams;

	}

	public List<League> createLeagues(List<League> leagues) {

		return leagues;

	}

	private static Player createPlayer(Player player) {

		return client.target("http://localhost:8080/league/webapi/player/").queryParam("name", player.getName())
				.queryParam("firstName", player.getFirstName()).queryParam("jerseyNumber", player.getJerseyNumber())
				.queryParam("status", player.getStatus()).queryParam("salary", player.getSalary())
				.request(MediaType.APPLICATION_JSON).get(Player.class);

	}

}
